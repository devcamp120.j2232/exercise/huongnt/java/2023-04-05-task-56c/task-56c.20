package com.devcamp.artistalbumapi.models;

import java.util.ArrayList;

public class Album {
    private int id;
    private String name;
    private ArrayList<String> song;

    
    public Album() {
    }

    
    public Album(int id, String name) {
        this.id = id;
        this.name = name;
    }


    public Album(int id, String name, ArrayList<String> song) {
        this.id = id;
        this.name = name;
        this.song = song;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public ArrayList<String> getSong() {

        return song;
    }


    public void setSong(ArrayList<String> song) {
        this.song = song;
    }



    
    
}
