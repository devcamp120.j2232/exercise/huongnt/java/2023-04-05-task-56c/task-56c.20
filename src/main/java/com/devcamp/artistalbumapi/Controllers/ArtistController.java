package com.devcamp.artistalbumapi.Controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.artistalbumapi.models.Album;
import com.devcamp.artistalbumapi.models.Artist;
import com.devcamp.artistalbumapi.service.AlbumService;
import com.devcamp.artistalbumapi.service.ArtistService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ArtistController {
    @Autowired
  
    private ArtistService artistService;
    @GetMapping("/artists")

    public ArrayList<Artist> getAllArtists(){
        ArrayList<Artist> allArtist = artistService.getAllArtists();
        return allArtist;
    }
    
    @GetMapping("/artist-info")
    public Artist getArtistInfoApi(@RequestParam(required = true, name = "artistId") int artistId){
        ArrayList<Artist> artistList = new ArrayList<>();
        artistList.addAll(artistService.getAllArtists());
        Artist artist = new Artist();
        for (int i=0; i< artistList.size(); i++){
            if (artistList.get(i).getId() == artistId){
                artist = artistList.get(i);
            }
        }
        return artist;
    }
    @Autowired
    private AlbumService albumService;
    @GetMapping("/album-info")
    public Album getAlbumInfoApi(@RequestParam(required = true, name = "albumId") int albumId){
        ArrayList<Album> albumList = new ArrayList<>();
        albumList.addAll(albumService.allAlbum());
        Album album = new Album();
        for (int i = 0; i < albumList.size(); i++){
            if (albumList.get(i).getId() == albumId){
                album = albumList.get(i);
            }
        }
        return album;
    }

    
}
